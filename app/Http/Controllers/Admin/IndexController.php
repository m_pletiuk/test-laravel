<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function show()
    {
        //$data = ['title'=>'Hello, World!!!'];
        //return view('default.template', $data);

        // return view('default.template')->with('title', 'Hello, Galia!');

        // $view = view('default.template');
        // $view->with('title', 'Hello, Galia!');
        // $view->with('title2', 'Hello, 2!');
        // $view->with('title3', 'Hello, 3!');
        // return $view;
        $array = [
            'title' => 'Laravel Project',
            'data' => [
                'one' => 'List 1',
                'two' => 'List 2',
                'three' => 'List 3',
                'four' => 'List 4',
                'five' => 'List 5',
            ],
            'dataI' => ['List 1','List 2','List 3','List 4','List 5'],

            'bvar' => true,
            'script' => "<script>alert('hello')</script>"
        ];


        if (view()->exists('default.index')) {

            // view()->name('default.template', 'myview');
            // return view()->of('myview')->withTitle('ashdkhasdb');

            // $view = view('default.template', ['title'=>'Hello-worlds'])->render();
            // echo $view;

            //$path = config('view.paths');
            //return view()->file($path[0].'/default/template.php')->withTitle('ashdkhasdb');
            //return view('default.template')->withTitle('ashdkhasdb');

            $data = ['title'=>'Hello, World!!!'];
            return view('default.index', $array);
        }
    }
}
