<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AboutController extends Controller
{
    //
    public function show()
    {
        //return 'hello';
        if (view()->exists('default.about')) {

            // select
            /*$articles = DB::select(
                "SELECT * FROM `articles` WHERE id = :id",
                [
                    'id' => 2,
                ]);*/

            // insert
            /*DB::insert("INSERT INTO `articles` (`name`, `text`, `img`) VALUES (:name, :text, :img)",
                [
                    'name' => 'test name',
                    'text' => 'test text',
                    'img' => 'test-img.png',
                ]);*/

            // update
            /*$result = DB::update("UPDATE `articles` SET `name` = :name WHERE `id` = :id",
                [
                    'name' => 'test 3333',
                    'id' => '5',
                ]);*/

            // delete
            /*$result = DB::delete("DELETE FROM `articles` WHERE id = :id", ['id' => 5]);*/

            /*DB::statement("DROP table ``articles");*/

            /*$result = DB::connection()->getPdo()->lastInsertId();*/

            /*$articles = DB::select("SELECT * FROM `articles`");
            dump($articles); // dd()*/
            // dump($result); // dd()

            $page = DB::select("SELECT `name`, `text` FROM `pages` WHERE `alias` = :alias", [
                'alias' => 'about',
            ]);
            return view('default.about')->with(['title' => 'About OUR Company&', 'page' => $page[0]]);


//            $view = view('default.about')->withTitle('HELLLLLLLO!!!!!!!!!')->render();
            /*return (new Response($view))
                ->header('Content-Type', 'newType')
                ->header('Header-One', 'Header One');*/
            /*return \response($view)
                ->header('Content-Type', 'newType')
                ->header('Header-Two', 'Header TWOOOOOOOOOO!');*/

//            return \response()->view('default.about',['title'=>'KASKAKSAKSKS'])->header('cacs','adsadad');

//            return \response()->download('robots.txt','mytext.txt');
//            return \response($view)->withHeaders(['header1' => 'one', 'header2' => 'two']);

//            return redirect('/')->withInput(); // передача в сесію всіх інпутів?
//            return redirect('/')->with('param1', 'Hello'); // передача в сесію
//            return redirect()->route('home');
//            return redirect()->action('Admin\ContactController@show');

//            return \response()->myRes('AHAHAHHAAHH');

        }
    }
}
