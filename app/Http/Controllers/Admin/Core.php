<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Country;
use App\Role;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Core extends Controller
{
    protected static $articles;

    //
    public function __construct()
    {
        //$this->middleware('mymiddle');
    }

    /**
     * @param $array
     * @return mixed
     */
    public static function addArticles($array)
    {
        return self::$articles[] = $array;
    }

    /**
     * @param $page
     */
    public function getArticle($page)
    {
        echo 'Ответ - ' . $page;
    }

    public function getArticles()
    {
//        $articles = Article::all();


//        $articles = Article::where('id', '>', 6)->get();

//        Article::chunk(2, function ($articles) {
//
//        });

//        $article = Article::find(1);

//        $article = Article::where('id', 1)->first();
//        echo $article->text;

//        $articles = Article::find([1,2]);

//        $articles = Article::findOrFail(222);
//        $articles = Article::where('id', 111)->firstOrFail();

//        $article = new Article();
//        $article->name = 'New Article';
//        $article->text = 'New Text';
//        $article->img = 'newImg.jpg';
//        $article->save();

//        $article = Article::find(21);
//        $article->name = 'New Article 2';
//        $article->text = 'New Text 2';
//        $article->img = 'newImg-2.jpg';
//        $article->save();

//        Article::create([
//            'name' => 'Hello Worlde!',
//            'text' => 'Some text',
//            'img' => '23.jpg',
//        ]);

//        $article = Article::firstOrCreate([
//            'name' => 'Hello Worlde!',
//            'text' => 'Some text',
//            'img' => '23.jpg',
//        ]);

//        $article = Article::firstOrNew([
//            'name' => 'Hello Worlde!3232323',
//            'text' => 'Some text',
//            'img' => '23.jpg',
//        ]);
//        $article->save();

//        $article = Article::find(24);
//        $article->delete();

//        Article::destroy(23);

//        Article::destroy([22, 21]);

//        Article::where('id','>', 3)->delete();

//        $article = Article::find(20); //softdelete - see model
//        $article->delete();

//        $articles = Article::withTrashed()->get();
//        $articles = Article::onlyTrashed()->restore();
//        $articles = Article::withTrashed()->restore();
//        foreach ($articles as $article) {
//            if ($article->trashed()) {
//                echo $article->id . ' deleted<br>';
//                $article->restore();
//            } else {
//                echo $article->id . ' not deleted<br>';
//            }
//        }

//        $article = Article::find(20);
//        $article->forceDelete();

//        foreach ($articles as $article) {
//            echo $article->text.'<br>';
//        }


//        dump($articles);
//        dump($article);

//        $user = User::find(1);
//        $articles = $user->articles()->where('id','>',5)->get();
//        $articles = $user->articles()->where('id','>',5)->get();

//        foreach ($articles as $article) {
//            echo $article->name.'<br>';
//        }

//        $article = Article::find(1);
//        dump($article->user->name);

//        $user = User::find(1);
//        $roles = $user->roles()->where('roles.id','2')->first();
//        foreach ($roles as $role) {
//            echo $role->name.'<br>';
//        }

//        $role = Role::find(1);

//        $articles = Article::all();
//        $articles->load('user');
//        foreach ($articles as $article) {
//            echo $article->user->name;
//        }

//        $users = User::with(['articles', 'roles'])->get();
//        foreach ($users as $user) {
//            dump($user->roles);
//            dump($user->articles);
//        }

//        $users = User::has('articles','>=',3)->get();
//        foreach ($users as $user) {
//            dump($user);
//        }

//        $user = User::find(2);
//        $article = new Article([
//            'name' => 'New article AAHHAHA',
//            'text' => 'New text AAHHAHA',
//            'img' => 'Newimg.jpg',
//        ]);

//        $user->articles()->save($article);
//        $user->articles()->create([
//            'name' => 'New article 2222222',
//            'text' => 'New text -----',
//            'img' => 'Newimg2.jpg',
//        ]);

//        $user->articles()->saveMany([
//            new Article([
//                'name' => 'article 2',
//                'text' => 'text 2',
//                'img' => 'imagi2.jpg',
//                ]),
//            new Article([
//                'name' => 'article 3',
//                'text' => 'text 3',
//                'img' => 'imagi3.jpg',
//                ]),
//            new Article([
//                'name' => 'article 4',
//                'text' => 'text 4',
//                'img' => 'imagi4.jpg',
//                ]),
//        ]);


//        $user = User::find(2);
//        $role = new Role([
//            'name' => 'guest',
//        ]);
//        $user->roles()->save($role);

        $user = User::find(2);

        $user->articles()->where('id', 19)->update(['name'=>'TITLE']);


        $articles = Article::find(19);
        dd($articles);


//        dump($articles);

    }


    /*public function getArticles()
    {
//        $articles = DB::table('articles')->get();
//        $articles = DB::table('articles')->get()->first();
//        $articles = DB::table('articles')->value('name');
//        DB::table('articles')->orderBy('id')->chunk(2, function ($articles) {
//            foreach ($articles as $article) {
//                Core::addArticles($article);
//            }
//        });
//        dump(self::$articles);

//        $articles = DB::table('articles')->pluck('name');
//        $articles = DB::table('articles')->count();
//        $articles = DB::table('articles')->max('id');
//        $articles = DB::table('articles')->select('name', 'id', 'text')->get();
//        $articles = DB::table('articles')->distinct()->select('name')->get();
//         $query = DB::table('articles')->select('name');
//         /////
//         $articles = $query->addSelect('text AS fulltext')->get();
//         /////

//        $articles = DB::table('articles')->select('text AS fulltext')->where('id', '=', 4)->get();

//        $articles = DB::table('articles')->select('name', 'text AS fulltext')->where('id', '=', 4)->where('name', 'like', 'test%', 'or')->get();

//        $articles = DB::table('articles')
//            ->select('name', 'text AS fulltext')
//            ->where([
//                ['id', '=', 4],
//                ['name', 'like', 'test%', 'or'],
//            ])
//            ->orWhere('name', 'like', '%blog%')
//            ->get();

//        $articles = DB::table('articles')
//            //->select('name', 'text AS fulltext')
////            ->whereBetween('id', [1, 5])
//            ->whereNotBetween('id', [1, 5])
//            ->get();
//        $articles = DB::table('articles')->whereIn('id', [1, 2, 3])->get();
//        $articles = DB::table('articles')->whereNotIn('id', [1, 2, 3])->get();
//        $articles = DB::table('articles') // Error with select
//            ->select('name')
//            ->groupBy('name')
//            ->get();

        //limit
//        $articles = DB::table('articles')->take(4)->get();
//        $articles = DB::table('articles')->take(4)->skip(2)->get();

//        DB::table('articles')->insert([
//            ['name' => 'ahahhaha', 'text' => 'AHAHAHHAHAHA text', 'img' => 'ahahha.jpg'],
//            ['name' => 'ahahhaha', 'text' => 'AHAHAHHAHAHA text', 'img' => 'ahahha.jpg'],
//        ]);

//        $result = DB::table('articles')->insertGetId(['name' => 'ahahhaha', 'text' => 'AHAHAHHAHAHA text', 'img' => 'ahahha.jpg']);

//        $articles1 = DB::table('articles')->where('id', 20)->update(['name' => 'Heqqqwwwqqqso!']);

//        $articles1 = DB::table('articles')->where('id', 19)->delete();


        // $articles = DB::table('articles')->select('name', 'id', 'text')->get();

        //dump($articles);
//        dump($articles1);
    }
    */


}
