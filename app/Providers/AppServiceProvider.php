<?php

namespace App\Providers;

use DB;
use Illuminate\Support\ServiceProvider;
use Blade;
use Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('myDir', function ($var) { // Реєструєм свою директиву яка буде доступна по @myDir

            return "<h1>New directive - $var</h1>";
        });

        Response::macro('myRes', function ($value) { // Реєструєм свій метод який доступний по response()->myRes();

//            return $value;
            return Response::make($value);

        });

        DB::listen(function ($query) {
            dump($query->sql);
//            dump($query->bindings);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
