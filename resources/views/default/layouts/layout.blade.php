<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>{{ $title }}</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>

<body>

@section('navbar')
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false"
                        aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name:</a>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('articles') }}">Articles</a>
                    </li>
                    <li>
                        <a href="{{ route('about') }}">About</a>
                    </li>
                    <li>
                        <a href="{{ route('article', ['id' => 10]) }}">Article</a>
                    </li>
                    <li>
                        <a href="{{ route('page') }}">Page</a>
                    </li>
                    </li>
                    <li>
                        <a href="{{ route('contact') }}">Contact</a>
                    </li>
                </ul>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </nav>
@show @section('header')
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1>
                {{ $title }}
            </h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called a
                jumbotron and
                three supporting pieces of content. Use it as a starting point to create something more unique.</p>
            <p>
                <a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a>
            </p>
        </div>
    </div>
@show


<div class="container">
    <!-- Example row of columns -->
    <div class="row">

        <div class="col-md-3">
            @section('sidebar')
                <div class="sidebar-module">
                    <h4>Archives</h4>
                    <ol class="list-unstyled">
                        <li>
                            <a href="#">March 2014</a>
                        </li>
                        <li>
                            <a href="#">February 2014</a>
                        </li>
                        <li>
                            <a href="#">January 2014</a>
                        </li>
                        <li>
                            <a href="#">December 2013</a>
                        </li>
                        <li>
                            <a href="#">November 2013</a>
                        </li>
                        <li>
                            <a href="#">October 2013</a>
                        </li>
                        <li>
                            <a href="#">September 2013</a>
                        </li>
                        <li>
                            <a href="#">August 2013</a>
                        </li>
                        <li>
                            <a href="#">July 2013</a>
                        </li>
                        <li>
                            <a href="#">June 2013</a>
                        </li>
                        <li>
                            <a href="#">May 2013</a>
                        </li>
                        <li>
                            <a href="#">April 2013</a>
                        </li>
                    </ol>
                </div>
            @show
        </div>

        @yield('content')


    </div>

    <hr>

    <footer>
        <p>&copy; 2016 Company, Inc.</p>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="../../dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>