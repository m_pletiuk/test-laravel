@extends('default.layouts.layout')


@section('header')

@endsection

@section('sidebar')

@endsection

@section('content')
    <div class="jumbotron">
        <div class="blog-post">
            @if($page)
                <h2>{{ $page->name }}</h2>
                {!! $page->text !!}
            @endif
        </div>
    </div>
@endsection