<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'Admin\IndexController@show']);
Route::get('/about', ['as' => 'about', 'uses' => 'Admin\AboutController@show']);

Route::get('/page/{id?}', function ($id = null) {
    echo $id;

    //return view('page');
})->name('page');

Route::get('/article/{id}', ['uses' => 'Admin\Core@getArticle', 'middleware' => 'mymiddle', 'as' => 'article']);
Route::get('/articles', ['uses' => 'Admin\Core@getArticles', 'as' => 'articles']);


Route::match(['get', 'post'], '/contact/{id?}', ['uses' => 'Admin\ContactController@show', 'as' => 'contact']);
