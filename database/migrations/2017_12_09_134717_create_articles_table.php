<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id'); // INT AUTO_INCREMENT PRIMARY KEY
            $table->string('name', 100); // VARCHAR 100
            $table->text('text'); // TEXT
            $table->string('img', 255); // VARCHAR 255


            $table->timestamps(); // created_at and updated_at // very useful


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
