<?php

use App\Article;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @uses \Illuminate\Support\Facades\DB
     *
     * @return void
     */
    public function run()
    {
        // 1 way
        DB::insert('INSERT INTO `articles` (`name`,`text`,`img`) VALUES (?, ?, ?)', [
            'Blog Post',
            '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',
            'pic1.jpg',
        ]);

        // 2 way
        DB::table('articles')->insert([
            [
                'name' => "Sample Blog Post 2",
                'text' => "<p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>",
                'img' => "pic2.png",
            ],
            [
                'name' => "Blog Post 3",
                'text' => "<p>3 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>",
                'img' => "pic3.png",
            ],
        ]);


        // 3 way
        Article::create([
            'name' => "Blog Post 4",
            'text' => "<p>4 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>",
            'img' => "pic4.png",
        ]);


    }
}
